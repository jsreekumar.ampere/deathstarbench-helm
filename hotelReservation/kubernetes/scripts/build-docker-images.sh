#!/bin/bash

cd $(dirname $0)/..


#EXEC="docker buildx"
EXEC="docker"

USER="igorrudyk1"

TAG="latest"

# ENTER THE ROOT FOLDER
cd ../
ROOT_FOLDER=$(pwd)
$EXEC create --name mybuilder --use

for i in hotelreservation #frontend geo profile rate recommendation reserve search user #uncomment to build multiple images
do
  IMAGE=${i}
  echo Processing image ${IMAGE}
  cd $ROOT_FOLDER
  #$EXEC build -t "$USER"/"$IMAGE":"$TAG" -f Dockerfile . --platform linux/arm64,linux/amd64 --push
  $EXEC build -t "$USER"/"$IMAGE":"$TAG" -f Dockerfile . 
  cd $ROOT_FOLDER

  echo
done


cd - >/dev/null
