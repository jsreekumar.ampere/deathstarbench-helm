############ HOME TIMELINE READ ##################
helm install social-network ./socialnetwork/ -n social-network \
  --set-string global.resources="requests: 
      cpu: "100m"
    limits:
      cpu: "1"" \
  --set-string post-storage-service.container.resources="requests: 
      cpu: "2"
    limits:
      cpu: "2"" \
  --set-string nginx-thrift.container.resources="requests: 
      cpu: "4"
    limits:
      cpu: "4"" \
  --set-string home-timeline-service.container.resources="requests: 
      cpu: "4"
    limits:
      cpu: "4"" \
  --set-string home-timeline-redis.container.resources="requests: 
      cpu: "4"
    limits:
      cpu: "4"" \
   --set home-timeline-redis.replicas=3 \
   --set nginx-thrift.replicas=8 \
   --set home-timeline-service.replicas=5 \
   --set post-storage-service.replicas=4 


kubectl apply -f ./home-read-hpa.yaml -n social-network

#### To view JAeger Traces #####
#kubectl port-forward --address 10.76.87.175 svc/jaeger 16686 -n social-network

#   --set home-timeline-redis.replicas=10 \  
#   --set nginx-thrift.replicas=8 \
#   --set home-timeline-service.replicas=15 \
#   --set post-storage-service.replicas=5 
