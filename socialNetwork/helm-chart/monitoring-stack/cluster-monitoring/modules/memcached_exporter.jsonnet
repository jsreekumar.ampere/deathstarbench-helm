local utils = import '../utils.libsonnet';
local k = import 'ksonnet/ksonnet.beta.4/k.libsonnet';

{
  _config+:: {
    namespace: 'monitoring',

    memcached: {
      ips: ['192.168.1.62'],
    },

    // Add custom dashboards
   // grafanaDashboards+:: {
    //  'apc-ups-dashboard.json': (import '../grafana-dashboards/apc-ups-dashboard.json'),
   // },
  },

  memcachedExporter+:: {
    serviceMonitor:
      utils.newServiceMonitor('memcached-exporter', $._config.namespace, { 'k8s-app': 'memcached-exporter' }, $._config.namespace, 'metrics', 'http'),

    service:
      local service = k.core.v1.service;
      local servicePort = k.core.v1.service.mixin.spec.portsType;

      local memcachedExporterPort = servicePort.newNamed('metrics', 9150, 9150);

      service.new('memcached-exporter', null, memcachedExporterPort) +
      service.mixin.metadata.withNamespace($._config.namespace) +
      service.mixin.metadata.withLabels({ 'k8s-app': 'memcached-exporter' }) +
      service.mixin.spec.withClusterIp('None'),

    endpoints:
      utils.newEndpoint('memcached-exporter', $._config.namespace, $._config.memcached.ips, 'metrics', 9150),
  },
}
