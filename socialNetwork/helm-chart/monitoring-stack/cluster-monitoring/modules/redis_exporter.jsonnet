local utils = import '../utils.libsonnet';
local k = import 'ksonnet/ksonnet.beta.4/k.libsonnet';

{
  _config+:: {
    namespace: 'monitoring',

    redis: {
      ips: ['192.168.1.62'],
    },

    // Add custom dashboards
   // grafanaDashboards+:: {
    //  'apc-ups-dashboard.json': (import '../grafana-dashboards/apc-ups-dashboard.json'),
   // },
  },

  redisExporter+:: {
    serviceMonitor:
      utils.newServiceMonitor('redis-exporter', $._config.namespace, { 'k8s-app': 'redis-exporter' }, $._config.namespace, 'metrics', 'http'),

    service:
      local service = k.core.v1.service;
      local servicePort = k.core.v1.service.mixin.spec.portsType;

      local redisExporterPort = servicePort.newNamed('metrics', 9121, 9121);

      service.new('redis-exporter', null, redisExporterPort) +
      service.mixin.metadata.withNamespace($._config.namespace) +
      service.mixin.metadata.withLabels({ 'k8s-app': 'redis-exporter' }) +
      service.mixin.spec.withClusterIp('None'),

    endpoints:
      utils.newEndpoint('redis-exporter', $._config.namespace, $._config.redis.ips, 'metrics', 9121),
  },
}
