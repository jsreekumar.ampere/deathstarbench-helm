############ COMPOSE POST SERVICE  ##################
helm install social-network ./socialnetwork/ -n social-network \
  --set-string global.resources="requests: 
      cpu: "100m"
    limits:
      cpu: "1"" \
  --set-string nginx-thrift.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "1"" \
  --set-string compose-post-service.container.resources="requests: 
      cpu: "2"
    limits:
      cpu: "8"" \
  --set-string user-timeline-mongodb.container.resources="requests: 
      cpu: "2"
    limits:
      cpu: "4"" \
  --set-string user-timeline-redis.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "4"" \
  --set-string home-timeline-redis.container.resources="requests: 
      cpu: "2"
    limits:
      cpu: "4"" \
  --set-string home-timeline-service.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "1"" \
  --set-string text-service.container.resources="requests: 
      cpu: "2"
    limits:
      cpu: "4"" \
  --set-string unique-id-service.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "1"" \
  --set-string user-mention-service.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "4"" \
  --set-string user-mongodb.container.resources="requests: 
      cpu: "4"
    limits:
      cpu: "4"" \
  --set-string user-memcached.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "1"" \
  --set-string user-service.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "2"" \
  --set-string user-timeline-service.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "1"" \
  --set-string url-shorten-service.container.resources="requests: 
      cpu: "4"
    limits:
      cpu: "8"" \
  --set-string url-shorten-mongodb.container.resources="requests: 
      cpu: "4"
    limits:
      cpu: "8"" \
  --set-string post-storage-service.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "1"" \
  --set-string post-storage-mongodb.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "1"" \
  --set-string media-service.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "1"" \
  --set-string social-graph-redis.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "1"" \
  --set-string social-graph-service.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "2"" \
  --set-string social-graph-mongodb.container.resources="requests: 
      cpu: "500m"
    limits:
      cpu: "1"" \
   --set nginx-thrift.replicas=3 \
   --set compose-post-service.replicas=4 \
   --set user-timeline-mongodb.replicas=3 \
   --set user-timeline-redis.replicas=3 \
   --set user-timeline-service.replicas=2 \
   --set home-timeline-redis.replicas=5 \
   --set home-timeline-service.replicas=2 \
   --set post-storage-service.replicas=2 \
   --set post-storage-mongodb.replicas=2 \
   --set text-service.replicas=2 \
   --set user-mongodb.replicas=1 \
   --set user-memcached.replicas=1 \
   --set user-mention-service.replicas=1 \
   --set url-shorten-mongodb.replicas=1 \
   --set url-shorten-service.replicas=1 \
   --set social-graph-redis.replicas=2 \
   --set social-graph-service.replicas=1 \


#kubectl apply -f ./hpa.yaml -n social-network

#### To view JAeger Traces #####
#kubectl port-forward --address 10.76.87.175 svc/jaeger 16686 -n social-network
