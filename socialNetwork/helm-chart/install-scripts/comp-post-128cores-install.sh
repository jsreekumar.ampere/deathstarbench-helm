############ COMPOSE POST SERVICE  ##################
helm install social-network ./socialnetwork/ -n social-network \
  --set-string global.resources="requests: 
      cpu: "100m"
    limits:
      cpu: "1"" \
  --set-string nginx-thrift.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "1"" \
  --set-string compose-post-service.container.resources="requests: 
      cpu: "2"
    limits:
      cpu: "8"" \
  --set-string user-timeline-mongodb.container.resources="requests: 
      cpu: "4"
    limits:
      cpu: "8"" \
  --set-string user-timeline-redis.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "4"" \
  --set-string home-timeline-redis.container.resources="requests: 
      cpu: "2"
    limits:
      cpu: "4"" \
  --set-string home-timeline-service.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "1"" \
  --set-string text-service.container.resources="requests: 
      cpu: "2"
    limits:
      cpu: "4"" \
  --set-string unique-id-service.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "1"" \
  --set-string user-mention-service.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "4"" \
  --set-string user-mongodb.container.resources="requests: 
      cpu: "4"
    limits:
      cpu: "4"" \
  --set-string user-memcached.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "1"" \
  --set-string user-service.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "2"" \
  --set-string user-timeline-service.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "1"" \
  --set-string url-shorten-service.container.resources="requests:
      cpu: "4"
    limits:
      cpu: "8"" \
  --set-string url-shorten-mongodb.container.resources="requests: 
      cpu: "4"
    limits:
      cpu: "8"" \
  --set-string post-storage-service.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "1"" \
  --set-string post-storage-mongodb.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "4"" \
  --set-string media-service.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "1"" \
  --set-string social-graph-redis.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "1"" \
  --set-string social-graph-service.container.resources="requests: 
      cpu: "1"
    limits:
      cpu: "2"" \
  --set-string social-graph-mongodb.container.resources="requests: 
      cpu: "500m"
    limits:
      cpu: "1"" \
   --set nginx-thrift.replicas=3 \
   --set compose-post-service.replicas=7 \
   --set user-timeline-mongodb.replicas=2 \
   --set user-timeline-redis.replicas=2 \
   --set user-timeline-service.replicas=2 \
   --set home-timeline-redis.replicas=7 \
   --set home-timeline-service.replicas=2 \
   --set post-storage-service.replicas=2 \
   --set post-storage-mongodb.replicas=2 \
   --set text-service.replicas=4 \
   --set user-mongodb.replicas=1 \
   --set user-memcached.replicas=2 \
   --set user-mention-service.replicas=5 \
   --set url-shorten-mongodb.replicas=2 \
   --set url-shorten-service.replicas=2 \
   --set social-graph-redis.replicas=2 \
   --set social-graph-service.replicas=2 \

