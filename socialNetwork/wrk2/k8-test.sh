rps=1000
while [ $rps -le 5000 ]; do
  mkdir -p comp_post_$rps
  #mkdir -p home_read_$rps
  cnt=1
  while [ $cnt -lt 6 ]; do

    # CLEANUP
    cd ../helm-chart
    ./uninstall.sh
    p=$(kubectl get pods --all-namespaces | egrep "Pending|Terminating" | wc -l)
    while [ $p -gt 0 ]; do
      p=$(kubectl get pods --all-namespaces | egrep "Pending|Terminating" | wc -l)
      kubectl delete pod -n social-network --all --grace-period=0 --force
      sleep 10
    done

    # DEPLOY
    sleep 10
    ./install.sh
    #./comp-post-install.sh
    #./home-read-install.sh
    p=10
    while [ $p -gt 1 ]; do
      p=$(kubectl get pods -n social-network | grep -v Running | awk '{print $1}' | wc -l)
      sleep 10
    done

    # INIT
    cd ../
    python3 ./scripts/init_social_graph.py --graph=socfb-Reed98 --ip=10.76.87.233 --port=32080
    cd wrk2

    # START DATA COLLECTORS
    #mkdir -p comp_post_$rps/data_$cnt
    #mkdir -p home_read_$rps/data_$cnt
    #cd ampere-system-profiler-master
    #./collect.sh -i5 -n60 -o ../../comp_post_$rps/data_$cnt &
    #./collect.sh -i5 -n60 -o ../../home_read_$rps/data_$cnt &
    #cd ../


    #RUN
    #./wrk -t80 -c10000 -d5m -R$rps  -L -s ./scripts-k8s/social-network/read-home-timeline.lua http://10.76.87.233:8080/wrk2-api/home-timeline/read > home_read_$rps/results_10k_$cnt 2>&1
    #./wrk -t80 -c10000 -d5m -R$rps  -L -s ./scripts-k8s/social-network/read-user-timeline.lua http://10.76.87.233:8080/wrk2-api/user-timeline/read > user_read_$cnt/results_10k_R$rps 2>&1
    ./wrk -t100 -c1000 -d5m -R$rps -L -s ./scripts-k8s/social-network/compose-post.lua http://10.76.87.233:32080/wrk2-api/post/compose > comp_post_$rps/results_1k_$cnt 2>&1
    sleep 30
    cnt=$(( $cnt + 1 ))
  done
  rps=$(( $rps + 200 ))
done

exit

