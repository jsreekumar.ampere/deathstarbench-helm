#!/bin/bash

# Copyright (C) 2018-2020 Ampere Computing Confidential Information
# All Rights Reserved.

###########################################################################
#THIS WORK CONTAINS PROPRIETARY INFORMATION WHICH IS THE PROPERTY OF
#Ampere Computing AND IS SUBJECT TO THE TERMS OF NON-DISCLOSURE AGREEMENT
#BETWEEN Ampere Computing AND THE COMPANY USING THIS FILE.
###########################################################################

cur_dir=`pwd`
cd $cur_dir
data_dir="../data"
collector_arr=("all")

# ======================
# VARIABLES
# ======================

# Source ASP.properties file
. ASP.properties

# ======================
# FUNCTIONS
# ======================
declare -A JOBS

clean_up() {
	echo "Test Terminated."

	for pid in ${!JOBS[@]}; do
        	#echo Clean ${JOBS[$pid]}
        	kill ${JOBS[$pid]} >/dev/null 2>&1
	done
	
	sleep 2
	cd $cur_dir
	python3 plot.py

	exit 0
}

trap clean_up SIGINT

print_progress_bar() {
	iter=`expr $1 / 10`

	for ((k = 0; k <= 10 ; k++))
	do
	    echo -n "Waiting for data collectors to complete... [ "
	    for ((i = 0 ; i <= k; i++)); do echo -n "#"; done
	   for ((j = i ; j <= 10 ; j++)); do echo -n " "; done
	   v=$((k * 10))
	   echo -n " ] "
	   echo -n "$v %" $'\r'
	   sleep $iter
	done
	echo
}

start_collectors(){

        echo "Data collection for $number_of_samples samples, $sample_interval second interval"
	cd collectors
	if [ ! -d $data_dir ]
	then
		echo output data dir does not exist...creating
		mkdir $data_dir
	fi
	rm -rf $data_dir/*
	
	index=0
	if [ "${collector_arr[0]}" = "all" ]; then
		collector_arr=()
		for f in ./*; do
			collector_arr[${#collector_arr[@]}]="$f"
		done
	fi
	for f in "${collector_arr[@]}"; do 
	        if test -f "$f"; then	
  			./$f $sample_interval $number_of_samples $data_dir&
		
			JOBS[$index]=$!
        		index=$(( index + 1 ))
		else
			echo "collector $f not found. skipping..."
		fi
	done

	total_time=$(($sample_interval * $number_of_samples))
	sleep 1
	print_progress_bar $total_time

	# sleep an extra 5 seconds to allow things to settle down
	echo Cleaning up...
	for pid in ${!JOBS[@]}; do wait ${JOBS[$pid]};done

	cd ../
	echo "Done!"
}

howtouse(){
	cat <<-'EOF'

	This script can work with or without parameters.
	Without parameters, just execute it --> ./collect.sh
	If you use parameters, this is the correct format:
		-n [samples]                  number of samples to take
		-i [interval]                 interval (in seconds) between each sample
		-c [collectors]               "all" runs all supported collectors. Supported: cpu,cpu_freq,cpu_power,io,irq_affinity,network,numastat,perf
		-o [path]                     absolute path directory for output data files
		-p                            disables the plotting
		-h                            help
	
	Examples:

		# Generate statistics 20 samples, collected every 2 seconds
		./collect.sh -n 20 -i 2 

	EOF
}


# ======================
# MAIN
# ======================

cat << EOF
################################################################################
#
#                    ▄██▄
#                   ▄█  █▄
#                  ▄█    █▄
#              ▄▄▄▄█  ▄▄▄ █▄
#           ▄█▀▀ ▄█      ▀██▄
#         ▄█▀   ▄█         ▀█▄
#
#    A M P E R E   C O M P U T I N G
#
# Ampere System Profiler
#
# Description:
# Utility to profile performance and power at a platform-level
# using typical OS utilities like sysstat
#
################################################################################
EOF


# Check for prerequisites
if [ -z "$network_interface" ]
then
	echo warning: variable network_interface is not set in ASP.properties. This is needed for IRQ data. Will proceed without this.
fi

if ! command -v sar > /dev/null 2>&1
then
	echo sar does not exist or is not in the PATH. Aborting...
	exit
fi

if ! command -v sensors > /dev/null 2>&1
then
	echo The lm_sensors package does not exist or is not in the PATH. CPU Power data will not be available.
	export SENSORS_NOT_FOUND=1
fi

if ! command -v python3 > /dev/null 2>&1
then
	echo python3 does not exist or is not in the PATH. Post-processing of the data will not be possible.
	export PYTHON_NOT_FOUND=1
fi

#if ! command -v route > /dev/null 2>&1
#then
#	echo route binary not found. Cannot determine default network interface. Skipping network data.
#	export NETWORK_NOT_FOUND=1
#fi

if ! command -v numastat > /dev/null 2>&1
then
	echo numastat does not exist or is not in the PATH. NUMA imbalance data will not be collected.
	export NUMASTAT_NOT_FOUND=1
fi

perf --help > /dev/null 2>&1
if  [ ! $? -eq 0 ]
then
	echo perf does not exist, is not in the PATH, or does not run. Skipping perf data collection.
	export PERF_NOT_FOUND=1
fi

if [ "$#" -eq 0 ];then
	echo -n "Please specify the number of samples to take-> "
	read number_of_samples
	echo -n "Please specify the sample interval (take sample every X seconds)-> "
	read sample_interval

elif [ "$#" -ne 0 ];then
	while getopts ":n:i:o:c:hpf" opt; do
		case $opt in
			"n")
				number_of_samples="$OPTARG"
				;;
			"i")
				sample_interval="$OPTARG"
				;;
			"o")
				data_dir="$OPTARG"
				;;
			"c")
				IFS=',' read -ra collector_arr <<< "$OPTARG"
				;;
                        "f")
                                export PERF_DISABLED=1
                                ;;
                        "p")
                                plot_disabled=1
                                ;;
			\?)
				echo "Invalid option: -$OPTARG" >&2
				howtouse
				exit 1
				;;
			:)
				echo "Option -$OPTARG requires an argument." >&2
				howtouse
				exit 1
				;;
			"h"|*)
				howtouse
				exit 0
				;;
		esac
	done

fi

# Begin collecting data with sar
start_collectors


if [ "$PYTHON_NOT_FOUND" != "1" ] && [ "$plot_disabled" != "1" ]
then
	# Call plot.py to generate the graphs
        python3 plot.py
fi


