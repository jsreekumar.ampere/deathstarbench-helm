#!/usr/bin/env python3

# Copyright (C) 2018-2020 Ampere Computing Confidential Information
# All Rights Reserved.

# =========================================================================
# THIS WORK CONTAINS PROPRIETARY INFORMATION WHICH IS THE PROPERTY OF
# Ampere Computing AND IS SUBJECT TO THE TERMS OF NON-DISCLOSURE AGREEMENT
# BETWEEN Ampere Computing AND THE COMPANY USING THIS FILE.
# =========================================================================


import os
import plotly.graph_objects as go
import pandas as pd
from plotly.subplots import make_subplots

# ======================
# FUNCTIONS
# ======================

def generate_graph():

    fig = make_subplots(rows=5, cols=2, shared_xaxes="columns", subplot_titles=("<b>CPU Utilization</b>", "<b>Per-Core Utilization</b>", "<b>CPU Power</b>", "<b>CPU Frequencies</b>","<b>Disk I/O</b>", "<b>NUMA Stats</b>", "<b>Network Utilization</b>", "<b>Top CPU Hotspots</b>", "<b>Interrupt Queue Mapping</b>"),vertical_spacing=0.04, 
                        specs=[[{"type": "xy"}, {"type": "xy"}],
                               [{"type": "xy"}, {"type": "xy"}],
                               [{"type": "xy"}, {"type": "xy"}],
                               [{"type": "xy"}, {"type": "table"}],
                               [{"type": "xy"}, None] ]) 

    # cpu
    try:
        cpu_util_df = pd.read_csv ('./data/cpu.dat', header=1, sep="\s+") # usecols=[0,2,4])
        user_time = go.Scatter(x=cpu_util_df.iloc[:,0], y=cpu_util_df['%user'], name="%User")
        sys_time = go.Scatter(x=cpu_util_df.iloc[:,0], y=cpu_util_df['%system'], name="%System")
        iowait_time = go.Scatter(x=cpu_util_df.iloc[:,0], y=cpu_util_df['%iowait'], name="%IOWait")
        fig.add_trace(user_time, row=1, col=1)
        fig.add_trace(sys_time, row=1, col=1)
        fig.add_trace(iowait_time, row=1, col=1)
    except IOError:
        print("could not find cpu.dat. Skipping CPU charts")

    try:
        # per-CPU utilization
        cpu_consolidated_df = pd.read_csv ('./data/cpu_consolidated.dat', header=0, sep="\s+") 
        cpu_ind_usr_util = go.Bar(x=cpu_consolidated_df['CPU'], y=cpu_consolidated_df['%user'], name="Per-core User Utilization %")
        cpu_ind_sys_util = go.Bar(x=cpu_consolidated_df['CPU'], y=cpu_consolidated_df['%system'], name="Per-core System Utilization %")
        fig.add_trace(cpu_ind_usr_util, row=1, col=2)
        fig.add_trace(cpu_ind_sys_util, row=1, col=2)

        cpu_freq_consolidated_df = pd.read_csv ('./data/cpu_freq_consolidated.dat', header=0, sep="\s+") 
        cpu_freq = go.Bar(x=cpu_freq_consolidated_df['CPU'], y=cpu_freq_consolidated_df['Frequency(MHz)'], name="Per-core Frequency")
        fig.add_trace(cpu_freq, row=2, col=2)

    except IOError:
        print("could not find cpu_consolidated.dat. Skipping individual CPU utilization charts")
    
    try:
        # Disk
        disk_df = pd.read_csv ('./data/iotransfer.dat', header=1, sep="\s+") 
        disk_read = go.Scatter(x=cpu_util_df.iloc[:,0], y=disk_df['bread/s']*0.5, name="KBytes read/sec")
        disk_write = go.Scatter(x=cpu_util_df.iloc[:,0], y=disk_df['bwrtn/s']*0.5, name="KBytes written/sec")
        fig.add_trace(disk_read, row=3, col=1)
        fig.add_trace(disk_write, row=3, col=1)
    except IOError:
        print("could not find iotransfer.dat. Skipping I/O charts")

    try:
        # IRQs
        irq_df = pd.read_csv ('./data/irq.dat', header=None, sep="\s+") 
        cols = len(irq_df.columns)
        x = pd.Series(range(0,cols))
        irq = go.Bar(x=x, y=irq_df.iloc[0,:], name="Network IRQ Distribution")
        fig.add_trace(irq, row=5, col=1)
    except IOError:
        print("could not find irq.dat. Skipping Interrupt Queue mapping chart")

    try:
        # network
        network_df = pd.read_csv ('./data/netinterface.dat', header=0, sep="\s+") 
        rx = go.Scatter(x=cpu_util_df.iloc[:,0], y=network_df['rxkB/s'], name="Receive KBytes/sec")
        tx = go.Scatter(x=cpu_util_df.iloc[:,0], y=network_df['txkB/s'], name="Transmit KBytes/sec")
        fig.add_trace(rx, row=4, col=1)
        fig.add_trace(tx, row=4, col=1)
    except IOError:
        print("could not find netinterface.dat. Skipping network charts")

    try:
        # perf 
        if os.path.exists('data/perf.dat'):
            os.system("perf report -i data/perf.dat  2>/dev/null 1>&1 | awk '(!/^#/ && !/^$/){if($1 >= 2) {x=x+$1; print $1\"\t\"$2\"/\"$3;}}END{print x \"\tOther\" }' | sed -e 's/^[ \t]*//' -e 's/%//g'  > data/perf.out")
        perfout = pd.read_csv ('data/perf.out', header=None, sep="\s+")
        #labels=perfout.iloc[:,1]
        #values=perfout.iloc[:,0]
        #perf = go.Pie(labels=labels, values=values, name="CPU Hotspots", direction="clockwise", hoverinfo="label+percent+name", textinfo="label+percent")
        perf = go.Table(header=dict(values=['%cycles', 'Module']), cells=dict(values=[perfout.iloc[:,0], perfout.iloc[:,1]])) 
        fig.add_trace(perf, row=4, col=2)
    except IOError:
        print("could not find perf data. Skipping hotspot table")
    except IndexError:
        print("parse perf data failed. Skipping hotspot table")


    # CPU power
    try:
        cpu_power_df = pd.read_csv ('./data/cpu_power.dat', header=None, sep="\s+") 
        num_sockets = len(cpu_power_df.columns) 
        for x in range(1, num_sockets):
            cpu_power = go.Scatter(x=cpu_util_df.iloc[:,0], y=cpu_power_df.iloc[:,x], name="CPU "+ str(x) + " Power (W)")
            fig.add_trace(cpu_power, row=2, col=1)
        fig.update_yaxes(title_text="CPU Power(W)", range=[0, cpu_power_df.iloc[:,1:num_sockets].max()], row=2, col=1)
    except IOError:
        print ("Could not find cpu_power.dat. Skipping CPU Power chart")

    # CPU freq
    #cpu_freq_df = pd.read_csv ('./data/cpu_freq.dat', header=0, sep="\s+") 
    #num_sockets = len(cpu_freq_df.columns) 
    #for x in range(0, num_sockets):
        #cpu_freq = go.Scatter(x=cpu_freq_df.iloc[0,:], y=cpu_freq_df.iloc[:,x], name="CPU "+ str(x) + " freq)")
        #fig.add_trace(cpu_freq, row=2, col=1)

    try:
        numa_df1 = pd.read_csv ('./data/numastat_start.dat', header=0, sep="\s+") 
        numa_df2 = pd.read_csv ('./data/numastat_end.dat', header=0, sep="\s+") 
        num_numa_nodes = len(numa_df1.columns) - 1

        for x in range(0, num_numa_nodes):
            numa_node = go.Bar(x=numa_df1.iloc[:,0], y=numa_df2['node' + str(x)]-numa_df1['node' + str(x)], name="Node" + str(x) + "  NUMA stats") 
            fig.add_trace(numa_node, row=3, col=2)

    except IOError:
        print ("Could not find numastat.dat. Skipping NUMA imbalance chart")


    fig.update_yaxes(title_text="%Utilization", range=[0, 100], row=1, col=1)
    fig.update_xaxes(title_text="CPU_num", row=1, col=2, dtick=1, showticklabels=True)
    fig.update_yaxes(title_text="Per-core %Utilization", range=[0, 100], row=1, col=2)
    fig.update_yaxes(title_text="I/O (KB/sec)", row=3, col=1)
    fig.update_xaxes(title_text="CPU_num", row=2, col=2, dtick=1, showticklabels=True)
    fig.update_yaxes(title_text="Average CPU Frequency (MHz)", row=2, col=2, tickformat=",d")
    fig.update_yaxes(title_text="Network I/O (KB/sec)", row=4, col=1)
    fig.update_yaxes(title_text="Number of network IRQs", row=5, col=1)
    fig.update_layout(title={'text':"<b>Ampere System Profiler</b>", 'font':{'color':"#f63823", 'size':24}},  height=2000, autosize=True)

    try:
        fig.write_html('report.html', auto_open=False)
        print ("Collection complete. Output => report.html");
    except:
        print ("Error writing out output file.")


# ======================
# MAIN
# ======================

if __name__ == '__main__':
    generate_graph()
