# Ampere System Profiler

The Ampere System Profiler (ASP) is a system-level analysis profiler that can be used to look for common platform-level problems while running workloads. It also includes a visualizer in the form of Plotly graphs and tables in an HTML report.
ASP can be used for workload characterization at a system level, a first step towards a balanced platform. 

## System pre-Requisites
Required Commands
 - sar
 - sensors
 - route
 - numastat
 - perf
 - python3
  
YUM Packages for CentOS or RHEL:
```shell
sudo yum install lm_sensors perf sysstat numactl net-tools python3
```
 
APT Packages for Ubuntu or Debian:
```shell
sudo apt install lm-sensors sysstat numactl net-tools linux-tools-common linux-tools-$(uname -r) python3
```

Python Modules:
```shell
python3 -m pip install plotly pandas
```

## Getting the Source Package
The source for ASP is distribute in a tar.gz format and should be provided either through GitLab (for internal distributions) orvia Customer Connect (for external distributions).

## Installation
Once you have the source locally in tar.gz format, unpack it using tar:  
```tar -xf ampere-system-profiler-x.x.x.tar.gz```
Then navigate to the root source folder:
```cd ampere-system-profiler-x.x.x```

## Profiling
Before profiling, please specify the network interface in ```ASP.properties```.  

Then follow the usage to collect profling data, ```sudo``` is required.  
```
./collect.sh -h
-n [samples]                  number of samples to take
-i [interval]                 interval (in seconds) between each sample
-c [collectors]               "all" runs all supported collectors. Supported: cpu,cpu_freq,cpu_power,io,irq_affinity,network,numastat,perf
-o [path]                     absolute path directory for output data files
-p                            disables the plotting
-h                            help
```
Example commands:
```
# generate 20 samples at 2 seconds interval, all collectors
sudo ./collect.sh -n 20 -i 2

# generate 20 samples at 1 seconds interval, only collect cpu utilization,cpu_freq and cpu_power
sudo ./collect.sh -n 20 -i 1 -c cpu,cpu_freq,cpu_power

# generate 20 samples at 2 seconds interval, all collectors, without plotting 
sudo ./collect.sh -n 20 -i 2 -p 
```

## Output
ASP will generate a html format report under the root directory of ASP.   
The report includes:  
- CPU Utilization
- Per-Core Utilization
- CPU Power
- CPU Frequencies
- Disk I/O
- Network Utilization
- NUMA Stats
- Top CPU Hotspots


![CPU Util](/images/cpu_util.jpg)
![CPU Util Histogram](/images/cpu_hist.jpg)
![CPU Package Power](/images/cpu_power.jpg)
![CPU Util Histogram](/images/cpu_hist.jpg)
![IRQ Distribution](/images/irq.jpg)
![Disk I/O](/images/disk.jpg)
![Network I/O](/images/network.jpg)
![Top CPU Hotspots](/images/perf.jpg)

