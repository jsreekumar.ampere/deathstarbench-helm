rps=1000
while [ $rps -le 3000 ]; do
  cnt=1
  while [ $cnt -lt 6 ]; do
    avg_lat=$(egrep "Latency" comp_post_$rps/results_1k_$cnt | grep -v Distr | sed 's/\s[ ]*/,/g' | cut -d "," -f3)
    p99=$(egrep "99.000%" comp_post_$rps/results_1k_$cnt | sed 's/\s[ ]*/,/g' | cut -d "," -f3)
    p999=$(egrep "99.900%" comp_post_$rps/results_1k_$cnt | sed 's/\s[ ]*/,/g' | cut -d "," -f3)
    p9999=$(egrep "99.990%" comp_post_$rps/results_1k_$cnt | sed 's/\s[ ]*/,/g' | cut -d "," -f3)
    p99999=$(egrep "99.999%" comp_post_$rps/results_1k_$cnt | sed 's/\s[ ]*/,/g' | cut -d "," -f3)
    tail_lat=$(egrep "100.000%" comp_post_$rps/results_1k_$cnt | sed 's/\s[ ]*/,/g' | cut -d "," -f2)
    qps=$(egrep "Requests" comp_post_$rps/results_1k_$cnt | sed 's/\s[ ]*/,/g' | cut -d "," -f2)
    mean=$(egrep "Mean" comp_post_$rps/results_1k_$cnt | sed 's/\s[ ]*/,/g' | cut -d "," -f3)
    sdev=$(egrep "StdDeviation" comp_post_$rps/results_1k_$cnt | sed 's/\s[ ]*/,/g' | cut -d "," -f7 | cut -d ']' -f1)
    cnt=$(( $cnt + 1 ))
    echo "$avg_lat,$p99,$p999,$p9999,$p99999,$tail_lat,$mean,$sdev,$qps"
  done
  echo ""
  rps=$(( $rps + 200 ))
done
exit


